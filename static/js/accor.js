jQuery('.accordion dt').click(function() {
    jQuery(this).find('i').toggleClass('fa-info-circle fa-times').closest('dt').next()
        .slideToggle().siblings('.accordion_content').slideup();
});

jQuery('.accordion_content').hide();

// Shows first "dt"
// $("dd").hide().first().show();

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

document.getElementById('tema').addEventListener('click', function() {
    document.body.classList.toggle('dark')
});