from django.urls import path
from . import views

urlpatterns = [
    path('', views.authView, name='auth'),
    path('login/', views.loginView, name='login'),
    path('logout/', views.logoutView, name='logout'),
    path('rahasia/', views.rahasia, name='rahasia'),
]