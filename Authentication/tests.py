from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import authView
from django.http.request import HttpRequest

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class Authentication_Test(TestCase):
    def test_apakah_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_url_tidak_ada(self):
        response = Client().get('/url-tidak-ada')
        self.assertEqual(response.status_code, 404)

    def test_story9_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_login_template(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_logout_template(self):
        response = Client().get('/story9/logout/')
        self.assertTemplateUsed(response, 'logout.html')
    
    def test_rahasia_template(self):
        response = Client().get('/story9/rahasia/')
        self.assertTemplateUsed(response, 'rahasia.html')

    def test_auth_function(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, authView)

    def test_judul(self):
        request = HttpRequest()
        response = authView(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Authentication', html_response)