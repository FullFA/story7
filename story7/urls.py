from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('accordion.urls')),
    path('story8/', include('AJAX.urls')),
    path('story9/', include('Authentication.urls')),
]
