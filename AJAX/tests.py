from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import ajax
from django.http.request import HttpRequest

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class AJAX_Test(TestCase):
    def test_apakah_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_url_tidak_ada(self):
        response = Client().get('/url-tidak-ada')
        self.assertEqual(response.status_code, 404)

    def test_AJAX_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_ajax_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, ajax)

    def test_judul(self):
        request = HttpRequest()
        response = ajax(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Muhammad Fiqri Adrian', html_response)