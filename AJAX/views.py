from django.shortcuts import render

# Create your views here.
def ajax(request):
    context = {
        'judul' : 'Muhammad Fiqri Adrian',
    }
    return render(request, 'story8.html', context)