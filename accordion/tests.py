from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import accor
from django.http.request import HttpRequest

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class Accordion_Test(TestCase):
    def test_apakah_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_url_tidak_ada(self):
        response = Client().get('/url-tidak-ada')
        self.assertEqual(response.status_code, 404)

    def test_accor_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'accor.html')

    def test_accor_function(self):
        found = resolve('/')
        self.assertEqual(found.func, accor)

    def test_judul(self):
        request = HttpRequest()
        response = accor(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Muhammad Fiqri Adrian', html_response)

    


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver' ,chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_ganti_tema(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)
        dark_theme_button = self.browser.find_element_by_id('tema')
        dark_theme_button.click()

        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')

        time.sleep(1)
        light_theme_button = self.browser.find_element_by_id('tema')
        light_theme_button.click()

        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')
