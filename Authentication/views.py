from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from django.contrib.auth import authenticate, login, logout

# Create your views here.

def authView(request):
    context = {
        'judul' : 'Authentication',
    }
    return render(request, 'story9.html', context)

def loginView(request):
    context = {
        'judul' : 'Login',
    }

    if request.method == "POST":
        username_login = request.POST['username']
        password_login = request.POST['password']

        user = authenticate(request, username=username_login, password=password_login)
        
        if user is not None:
            login(request, user) 
            return redirect('rahasia')
        else:
            return redirect(request, 'login.html')
        

    return render(request, 'login.html', context)

def logoutView(request):
    context = {
        'judul' : 'Logout'
    }

    if request.method == "POST":
        if request.POST["logout"] == "Submit":
            logout(request)

            return redirect('/story9/')

    return render(request, 'logout.html', context)

def rahasia(request):
    context = {
        'judul' : 'Ini Rahasia'
    }

    return render(request, 'rahasia.html', context)